%!TEX root = ../ecir2017-fp-rolf.tex

\section{Implementation}
\label{sec:implementation}

In this section, we will discuss the implementation details of the asynchronous parameter server and the \OurLDA{} algorithm in Spark. We will review several important properties of our proposed system such as load balancing and fault tolerance.

\subsection{Parameter Server implementation}
We implemented a high-perfor\-mance asynchronous parameter server\footnoteGlint{}  in the Scala programming language \cite{odersky2004overview} that uses the Akka framework. Akka is ``a toolkit and runtime for building highly concurrent, distributed, and resilient message-driven applications''~\cite{akka}. It provides high-level abstractions that simplify much of the network communication while retaining high performance. We chose to use Scala to ensure compatibility with Spark, which itself is written in Scala. Although our implementation was developed for the purpose of collapsed Gibbs sampling and count tables, it can be applied much more broadly and is potentially useful for a variety of large-scale machine learning tasks that are known to benefit from the parameter server architecture such as sparse logistic regression~\cite{li2014scaling} and deep learning~\cite{dean2012large}.

The parameter servers store their data in main memory. Since fast random updates are necessary it uses a dense representation. Partial matrices are represented by two-dimensional arrays which are stored in row-major order. Vectors are represented by single-dimensional arrays. Since the system is implemented in Scala, we use the Java Virtual Machine (JVM) to run it. Internally, the JVM can store arrays of primitives as contiguous blocks of memory. By carefully implementing the parameter server to use the appropriate specialized primitives, we prevent boxing and thus garbage collection overhead. This is especially important since the parameter server should have high throughput and cannot afford long `stop-the-world' garbage collection routines, which would effectively stop all user code to run garbage collection~\cite{jvmgc}. For large JVM heap sizes this could take an unacceptable amount of time, making the parameter server seem unresponsive.

\subsection{LDA implementation}
We have implemented the \OurLDA{} algorithm using Spark and the asynchronous parameter server. A general overview of the implementation is provided in Fig.~\ref{fig:overview}. The Spark driver distributes the Resilient Distributed Dataset (RDD) of samples to different workers. Each worker pulls parts of the model from the parameter server and constructs corresponding alias tables. The worker then iterates over its local partition of the data and resamples the tokens using the Metropolis-Hastings algorithm. Updates are pushed asynchronously to the parameter server while the algorithm is running.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{illustrations/overview.pdf}
	\caption{Overview of the implementation. A dataset is split up into different partitions by Spark. Each partition of the data is resampled using the Metropolis-Hastings algorithm. Updates are pushed asynchronously to the parameter server.}
	\label{fig:overview}
\end{figure}

\subsection{Implicit load balancing}
\label{sec:loadbalance}
As the topic model gets distributed to multiple machines, it is important that the load per machine is balanced. The frequency of words in a corpus has been frequently shown to follow Zipf's law \cite{piantadosi2014zipf}. This means that a word's frequency is inversely proportional to its frequency rank. If the topic-word count matrix is distributed na\"{i}vely, placing all the most frequently occurring words on the same machine, it would form a severe bottleneck.

To help resolve this issue we order the features for our bag-of-words vectors by their respective frequency. This means that the most commonly occurring word is represented by the feature at index 1, the second most common word would be at index 2, etc. An additional preprocessing step is required to sort the features. Fortunately this is very easily integrated in the processing pipeline using Spark's {\sc map}, {\sc reduceByKey} and {\sc sortByKey} operators.  The parameter servers store rows in a cyclical fashion.  This means that the first row will be stored on the first parameter server, the second row on the second parameter server, etc. As a result the topic-word distribution for the most common word in the corpus will be stored on the first machine. The topic-word distribution of the second most common word will be stored on the second machine, and so forth. This process gives the system implicit load balancing. Fig.~\ref{fig:balance} shows that our load balancing scheme works better than randomly shuffling the features.

\begin{figure}
	\centering
        \vspace*{-2\baselineskip}
	\begin{minipage}{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth,trim=0cm 2.5cm 0cm 1.5cm]{illustrations/loadrandom.pdf}
		\small{Shuffled features\\\quad}
	\end{minipage}
	\begin{minipage}{0.15\textwidth}
		\quad
	\end{minipage}
	%\begin{minipage}{0.32\textwidth}
	%	\centering
	%	\includegraphics[width=\textwidth,trim=0cm 2.5cm 0cm 1.5cm]{illustrations/loadunbalance.pdf}
	%	\small{Ordered features\\Range partitioning}
	%\end{minipage}
	\begin{minipage}{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth,trim=0cm 2.3cm 0cm 0.4cm]{illustrations/loadbalance.pdf}
		\small{Ordered features\\Cyclical partitioning}\vspace{0.5cm}
	\end{minipage}
	\vspace{-1.7\baselineskip}
	\caption{Load balancing property of using frequency-ordered feature vectors. Each figure displays the expected proportion of requests per machine for 30 machines based on the corpus token counts. We either randomly shuffle the features or use our proposed load-balancing scheme that cyclically orders the features by frequency. The latter performs best.}
	\label{fig:balance}
	\vspace*{-\baselineskip}
\end{figure}

\if0
\subsection{Buffering}
In order to improve the run time of the system we use buffering. The most time consuming actions are the push requests to the parameter servers. It is infeasible to push every topic reassignment individually because there are billions of them per iteration. On the opposite end of the spectrum, we do not want to buffer all push requests and send them in one go, as the message size would be much too large. The system uses a middle ground and buffers approximately 100,000 topic reassignments, which constitutes a message size of about 2 megabytes. Having small messages is good because it makes resending the messages in case of network failure much less penalizing.s

Additionally, we use a special buffer for the most common words in the corpus. As we will see below, the words in the corpus follow a Zipfian distribution. Hence, certain words will be much more frequent than others. The topic reassignments for the top 2000 most frequent words are aggregated locally in a dense matrix, before sending them to the parameter servers once at the end of the iteration. 
\fi


%\subsection{Pipelining requests}
%The algorithm needs to pull the model from the parameter server in order to construct alias tables and perform resampling. Rows are pulled in fixed-size sets after which the algorithm can resample the corpus' tokens using the pulled part of the model. During the computationally intensive resampling stage, the next set of rows is already pulled on a separate network thread. By the time the algorithm finishes resampling the current part of the model, it can immediately continue to the next part of the model.

\subsection{Fault tolerance}
Despite the fact that the parameter servers themselves are not fault tolerant, we can still achieve fault tolerance as part of the implementation of the algorithm. We checkpoint the topic assignments $z$ to a redundant file storage after each iteration. If during one of the iterations the computation fails, the system will load the most recently checkpointed data set and rebuild the count table on the parameter servers. After this reconstruction the algorithm can continue computing the topic model.

