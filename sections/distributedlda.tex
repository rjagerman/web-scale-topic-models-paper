%!TEX root = ../ecir2017-fp-rolf.tex

\section{\OurLDA}
\label{sec:distributedlda}

%\begin{itemize}
%\item \todo{Proposed storyline}
%\begin{itemize}
%\item \todo{Start with \OurLDA, highlight differences with lightLDA, make clear where the asynchronous %parameter server model comes in}
%\item \todo{Then, in the next section present the asynchronous parameter server model}
%\item \todo{Separate algorithm, mode, implementation, experiment -- these four seem all mixed up in the %current sections 3 and 4.}
%\item \todo{Add a section implementation, that does exactly that: detail the implementation}
%\end{itemize}
%\end{itemize}

\OurLDA, our distributed version of LDA, builds on the LightLDA algorithm~\cite{yuan2015lightlda}; it uses an asynchronous version of the parameter server model, as we will detail in Section~\ref{sec:parameterserver}. LightLDA is a Collapsed Gibbs sampling~\cite{griffiths2004finding} algorithm that uses a Metropolis-Hastings approximation~\cite{metropolis1953equation}. Collapsed Gibbs sampling for LDA is a Markov Chain Monte-Carlo type algorithm that assigns a topic $z \in \{1 \ldots K\}$ to every token in the corpus. It then repeatedly re-samples the topic assignments $z$. To do this, we need to keep track of the statistics $n_{k}$, $n_{wk}$ and $n_{dk}$:
%
\begin{itemize}
    \item $n_k$: Number of times any word was assigned topic $k$
    \item $n_{wk}$: Number of times word $w$ was assigned topic $k$
    \item $n_{dk}$: Number of times a token in document $d$ was assigned topic $k$
\end{itemize}
%
It is clear that the document-topic counts $n_{dk}$ are document-specific and thus local to the data and need not be shared across workers. However, the word-topic counts $n_{wk}$ and topic counts $n_k$ are global and require sharing. The parameter server provides a shared interface to these values in the form of a distributed matrix storing $n_{wk}$, and a distributed vector storing $n_k$.

LightLDA factorizes the original collapsed Gibbs distribution into two parts: a document proposal distribution $P_d$ and a word proposal distribution $P_w$ (see~\eqref{eq:gibbs}). Drawing from $P_w$ in $\mathcal{O}(1)$ is achieved by using alias tables \cite{vose1991linear}, while drawing from $P_d$ in $\mathcal{O}(1)$ is achieved by using the topic assignments $z$ in a document directly. This is important because sampling billions of tokens is computationally infeasible if every sampling step would use $\mathcal{O}(K)$ operations, where $K$ is a potentially large number of topics. LightLDA provides corresponding acceptance probabilities $\pi_d$ and $\pi_w$.
%
\begin{equation}
P(z = k) \propto \underbrace{(n_{dk}^{-dw} + \alpha)}_{P_d} \underbrace{\frac{n_{wk}^{-dw} + \beta}{n_k^{-dw} + V\beta}}_{P_w}
\label{eq:gibbs}
\end{equation}
%

Despite its attractive properties, LightLDA has an important shortcoming. It uses a standard stale-synchronous parameter server model in which push requests are batched together and sent once when the algorithm finishes processing its current partition of the data. This architecture forces the push and pull requests to be processed on a synchronous network thread. In contrast, our approach sends push requests \emph{asynchronously} during the compute stage. These more frequent but smaller updates have a number of essential advantages. First of all, they decrease the staleness of the model while it is computing. With our approach it is possible to see updates from other machines within the same iteration over the data, something that is not possible with the standard parameter server model. Second, it makes mitigating network failure easier as small messages can be resent more efficiently. And third, it enables the algorithm to take advantage of more dynamic threading mechanisms such as fork-join pools and cached thread pools \cite{welc2005safe}. The move from such a fixed threaded design to a fully asynchronous parameter server model requires a re-design of LightLDA. Below we introduce \OurLDA{} while the asynchronous parameter server model is detailed in Section~\ref{sec:parameterserver}.

Algorithm~\ref{alg:lightlda} describes the \OurLDA{} method. At the start of each iteration, the algorithm performs a synchronous pull on each processor $p$ to get access to the global topic counts $n_k$. It then iterates over the vocabulary terms, and asynchronously pulls the word-topic counts $n_{wk}$ (line~\ref{lst:asynclda:syncpull}). These asynchronous requests call back the {\sc Resample} procedure when they complete. The {\sc Resample} procedure (line~\ref{ourlda:11}) starts by computing an alias table on the available word-topic counts $n_{wk}$. It then iterates over the local partition of the data $\mathcal{D}_p$ where it resamples every (token, topic) pair using LightLDA's $\mathcal{O}(1)$ Metropolis-Hastings sampler. Changes to the topic counts are pushed asynchronously to the parameter server while it is computing.

Note that all of our push requests either increment or decrement the counters $n_{wk}$ and $n_k$. The parameter server model exploits this fact by aggregating these updates via addition, which is both commutative and associative. This eliminates the need for complex locking schemes that are typical in key-value storage systems. Instead, the updates can be safely aggregated through an atomic integer structure that is easy to implement.

\begin{algorithm}[t]
	\begin{algorithmic}[1]
	\State {$\mathcal{P} \leftarrow $ Set of processors},\\
	{$\mathcal{D} \leftarrow $ Collection of documents},\\
	{$\mathcal{V} \leftarrow $ Set of vocabulary terms}
	\Statex
	\For{$p \in \mathcal{P}$ in parallel}
	  \State $\mathcal{D}_p \subseteq \mathcal{D}$
	  \State $n_k \leftarrow $ {\sc SyncPull}($\{n_k \mid k = 1 \dots K \}$) \label{lst:asynclda:syncpull}
	  \For{$w \in \mathcal{V}$}
		  \State{\bf on} {\sc AsyncPull}($\{n_{wk} \mid k = 1 \dots K \}$) {\bf call} {\sc Resample($\mathcal{D}_p, n_{wk}, n_k$)} \label{lst:asynclda:asyncpull}
	  \EndFor
	\EndFor
	\Statex
	\Procedure {Resample}{$\mathcal{D}_p, n_{wk}, n_k$}\label{ourlda:11}
	\State{$a \leftarrow $ AliasTable($n_{wk}$)}
	\For{$(w, z_{\text{old}}) \in d \in \mathcal{D}_p$}
	\State{$z_{\text{new}} \leftarrow$ MetropolisHastingsSampler($a, d, w, z_{\text{old}}, n_k, n_{wk}$)}\label{lst:asynclda:lightlda}
	\State{\sc AsyncPush}($\{n_{wk} \leftarrow n_{wk} + 1\}, \{n_k \leftarrow n_k + 1\}$) for $k = z_{\text{new}}$ \label{lst:asynclda:asyncpush1}
	\State{\sc AsyncPush}($\{n_{wk} \leftarrow n_{wk} - 1\}, \{n_k \leftarrow n_k - 1\}$) for $k = z_{\text{old}}$\label{lst:asynclda:asyncpush2}
	\EndFor
	\EndProcedure
	\end{algorithmic}
	\caption{\OurLDA: a distributed LightLDA algorithm.}
	\label{alg:lightlda}
\end{algorithm}

In the next section, we will discuss the asynchronous parameter server model that makes the implementation of this algorithm possible.