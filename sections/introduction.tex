%!TEX root = ../ecir2017-fp-rolf.tex

\section{Introduction}
\label{sec:introduction}

Probabilistic topic models are a useful tool for discovering a set of latent themes that underlie a text corpus. Each topic is represented as a multinomial probability distribution over a set of words, giving high probability to words that co-occur frequently and small probability to those that do not. Topic models have found broad usage in information retrieval, ranging from smoothing and feedback methods~\citep{yi-comparative-2009}, to tools for exploratory search and discovery~\citep{zhao-comparing-2011}, and for explaining clustering and recommendation results~\citep{zhao-explainable-2016}. % simple example in introduction

Recent information retrieval applications often require very large-scale topic modeling to boost their performance~\citep{yuan2015lightlda}, where many 1000s of topics are learned from terabyte-sized corpora (see Table~\ref{tbl:example} for an example). Classical inference algorithms for topic models do not scale well to very large data sets. This is unfortunate because, like many other machine learning methods, topic models would benefit from a large amount of training data. When trying to compute a topic model on a Web-scale data set in a distributed setting, we are confronted with a major challenge:

%\vspace{0.1cm}
\begin{center}\emph{How do individual machines keep their model synchronized?}\end{center}
%\vspace{0.1cm}

To address this issue, various distributed approaches to LDA have been proposed.
The state-of-the-art approaches rely on custom strategies, implementations and hardware to facilitate their asynchronous, commu\-nication-intensive workloads~\cite{chen2015warplda,yu2015scalable,yuan2015lightlda}. These highly customized implementations are difficult to use in practice because they are not easily integrated in today's data processing pipelines.

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		\textbf{Topic \#984} & \textbf{Topic \#405} & \textbf{Topic \#941} & \textbf{Topic \#741} \\ \hline
		`Tourism' &  `Video games' & `Javascript' & `Genetic Biology'  \\ \hline
		hotel &  allianc & write &  tag  \\ 
		locat & hord & docum & protein \\
		car & euro & var  & hypothet \\
		holidai & warcraft & return & gene \\
		area & wow & prop & cytoplasm  \\
		golf & gold & function & prk \\
		wed & warhamm & substr & locu \\
		room & starcraft & indexof & note \\ \hline
	\end{tabular}
	\vspace{0.3cm}
	\caption{Examples from the 1,000 topics computed on ClueWeb12~\cite{clueweb12}, a 27TB corpus.}
	\label{tbl:example}
	\vspace{-0.6cm}
\end{table}

We propose \OurLDA{} (Asynchronous Parameter Server LDA), a distributed version of LDA that builds on a widely used \CompFramework{}, Spark~\citep{zaharia2012resilient}. The advantages of integrating model training with existing \CompFramework{s} include convenient usage of existing data-processing pipelines and eliminating the need for intermediate disk writes since data can be kept in memory from start to finish~\citep{moritz-sparknet-2016}. However, Spark is bound to the typical map-reduce programming paradigm. Common inference algorithms for LDA, such as collapsed Gibbs sampling, are not easily implemented in such a paradigm since they rely on a large mutable parameter space that is updated concurrently. We address this by adopting the para\-meter server model~\citep{li2014scaling}, which provides a distributed and concurrently accessed para\-meter space for the model being learned.

% highlight contribution
Our unique contribution in this regard is that we propose an \emph{asynchronous} parameter server: individual pull and push requests are executed asynchronously inside a user-specified execution context. This allows for more dynamic scheduling of requests and highly granular updates that prevent model staleness. Our goal is not to outperform highly customized computational frameworks but to propose a topic modeling approach that can easily be implemented in a popular \CompFramework{} and that is highly scalable in both dataset size and number of topics. 

\if0
\todo{To do: Add research questions? These are the best I have for now, but they feel a bit forced, thinking of better way to phrase this.
	\begin{itemize}
	\item Can we compute an LDA model with 1000s of topics on Web-scale corpora using a limited cluster of 30 machines? (highly customized implementations can already do this, we want to generalize to batch processing frameworks such as Spark)
	\item How do we achieve fault-tolerance, straggler mitigation and load balancing without complicating our design and implementation? (by using Spark)
	\item Does our solution perform more efficiently than similar implementations without sacrificing model quality? (answered by the experiments)
\end{itemize}
}
\fi

\if0
\todo{\\{\em This is my original Master thesis proposal, perhaps it can help to come up with some concise research questions that are answered in the paper:}\\\\
	``In many machine learning applications we wish to compute a model that is too large to fit in the memory of a single machine. To resolve this issue, recent advances in distributed machine learning have focused on the concept of a parameter server. Such a parameter server distributes a large model across multiple machines. Workers that are performing computation on data can query these machines to obtain and update slices of the model asynchronously. Existing implementations of parameter servers are written in C++. This makes them hard to use in practice, as the difficulty curve of C++ is quite steep.\\\\
	Spark is a map-reduce framework that has seen great success in recent years. It provides the building blocks to construct large-scale data processing applications in Scala, Java and Python. It exposes a very easy to use high-level API through a concept called ”Resilient Distributed Dataset” (RDD). Although this is a very powerful abstraction, it is restricted to the map-reduce programming paradigm which is inherently synchronous.\\\\
	The goal of this master thesis is to combine these two technologies. We wish to use the high-level abstractions offered by Spark and combine them with the asynchronous parameter server technology. To accomplish this, a new implementation of the parameter server will be written in Scala. It will utilize Akka to reduce much of the complexity involving distributed computation and networking. The parameter server will expose an easy to use API to the algorithm, allowing it to ”pull” and ”push” slices of the model. The system will be designed in such a way that it is easily integrated with Spark.\\\\
	As a proof of concept this parameter server will be used to train a large-scale LDA model on the ClueWeb12 corpus. Since network traffic is typically the limiting factor in distributed computation, we will focus on optimizing communication overhead. In the context of LDA we can exploit some of the properties of natural text (e.g. the inherent power-law distribution of words) to, for example, prioritize certain model slices over others.''
}
\fi
\if0
The remainder of this paper is structured as follows: Section~\ref{sec:parameterserver} describes the architecture of the Spark-compatible parameter server. Section~\ref{sec:distributedlda} provides a short description of the LightLDA algorithm and our implementation of it in Spark. The setup and results of the experiments are described in Section~\ref{sec:experiments}. Finally, the paper is concluded in Section~\ref{sec:conclusion}.
\fi