%!TEX root = ../ecir2017-fp-rolf.tex

\section{An Asynchronous Parameter Server Model}
\label{sec:parameterserver}

The traditional parameter server model~\cite{li2013parameter} is a complete machine learning framework that includes task scheduling, a distributed (key, value) store for the parameters and user-defined functions that can be executed on workers and servers. As a result, there is considerable complexity in the design and implementation of a working parameter server, making it difficult to use in practice. The asynchronous parameter server model we propose is decoupled from task scheduling and can easily be used with existing \CompFramework{s}. This is accomplished by simplifying the parameter server interface to a set of two operations:

\begin{enumerate}
	\item \textbf{Asynchronously `Pull' data from the servers.}\\This will query parts of the matrix or vector.
	\item \textbf{Asynchronously `Push' data to the servers.}\\This will update parts of the matrix or vector.
\end{enumerate}

The goal of our parameter server model is to store a large distributed matrix and provide a user with fast queries and updates to this matrix. In order to achieve this it will partition and distribute the matrix to multiple machines. Each machine only stores a subset of rows. An algorithm interacts with the matrix through the \textit{pull} and \textit{push} operations and is unaware of the physical location of the data. By using the parameter server model, the algorithm acts solely on a virtual view of the matrix (see Fig.~\ref{fig:partialmatrix}).

\begin{figure}
	\centering
	\includegraphics[width=0.65\textwidth]{illustrations/partialmatrix.pdf}
	\caption{Layout of a distributed matrix on the parameter servers. The user only interacts with the matrix through basic pull and push actions.}
	\label{fig:partialmatrix}
\end{figure}

\subsection{Pull action}
Whenever an algorithm wants to retrieve entries from the matrix it will call the \textit{pull} method. This method triggers an asynchronous pull request with a specific set of row and column indices that should be retrieved. The request is split up into smaller requests based on the partitioning of the matrix such that there will be at most one request per parameter server.

Low-level network communication provides an `at-most-once' guarantee on message delivery. This is problematic because it is impossible to know whether a message sent to a parameter server is lost or just takes a long time to compute. However, since pull requests do not modify the state of the parameter server, we can safely retry the request multiple times until a successful response is received. To prevent flooding the parameter server with too many requests, we use an exponential back-off timeout mechanism. Whenever a request times out, the timeout for the next request is increased exponentially. If after a specified number of retries there is still no response, we consider the pull operation failed.

\subsection{Push action}
In contrast to pull requests, a \textit{push} request will modify the state on the parameter servers. This means we cannot na\"{i}vely resend requests on timeout because if we were to accidentally process a push request twice it would result in a wrong state on the parameter server. We created a hand-shaking protocol to guarantee `exactly-once' delivery on push requests with minimal overhead (see Fig.~\ref{fig:pushrequest}). The protocol first attempts to obtain a unique transaction $id$ for the push request. Data is transmitted together with the transaction $id$, allowing the protocol to later acknowledge receipt of the data. The protocol only uses a timeout and retry mechanism on messages that are guaranteed not to affect the state of the parameter server. The result is that pushing data to the parameter servers happens exactly once.

\begin{figure*}
	\centering
	\includegraphics[width=0.95\textwidth,trim=0mm 0mm 0mm 2cm]{illustrations/pushrequest.pdf}
	\caption{Hand-shake protocol that guarantees exactly-once delivery for push requests.}
	\label{fig:pushrequest}
\end{figure*}
